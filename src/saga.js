import { all } from 'redux-saga/effects';
import { usersSagas } from './features/users';

export default function* rootSaga() {
  yield all([...usersSagas]);
}
