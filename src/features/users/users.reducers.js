import { USERS_ACTIONS } from './users.actions';
import { INITIAL_USERS_STATE } from './users.constants';

export default (state = INITIAL_USERS_STATE, action) => {
  switch (action.type) {
    case USERS_ACTIONS.GET_USERS_SUCCESS: {
      const { items } = action;
      return {
        ...state,
        items,
      };
    }
    case USERS_ACTIONS.SET_USERS_ERROR: {
      const { error } = action;
      return {
        ...state,
        error,
      };
    }
    default: {
      return state;
    }
  }
};
