import axios from 'axios';
import { USERS_NAMESPACE } from './users.constants';

export const getUsers = () =>
  axios.get(USERS_NAMESPACE, {
    params: {
      limit: 1000,
    },
  });

export const createUser = (user) => axios.post(USERS_NAMESPACE, user);

export const deleteUser = (userId) => axios.delete(`${USERS_NAMESPACE}/${userId}`);
