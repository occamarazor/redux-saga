import { USERS_NAMESPACE } from './users.constants';

export const USERS_ACTIONS = {
  GET_USERS_REQUEST: `${USERS_NAMESPACE}/get_users_request`,
  GET_USERS_SUCCESS: `${USERS_NAMESPACE}/get_users_success`,
  DELETE_USER_REQUEST: `${USERS_NAMESPACE}/delete_user_request`,
  CREATE_USER_REQUEST: `${USERS_NAMESPACE}/create_user_request`,
  SET_USERS_ERROR: `${USERS_NAMESPACE}/user_error`,
};

export const getUsersRequest = () => ({
  type: USERS_ACTIONS.GET_USERS_REQUEST,
});

export const getUsersSuccess = (items) => ({
  type: USERS_ACTIONS.GET_USERS_SUCCESS,
  items,
});

export const createUserRequest = (user) => ({
  type: USERS_ACTIONS.CREATE_USER_REQUEST,
  user,
});

export const deleteUserRequest = (userId) => ({
  type: USERS_ACTIONS.DELETE_USER_REQUEST,
  userId,
});

export const setUsersError = (error) => ({
  type: USERS_ACTIONS.SET_USERS_ERROR,
  error,
});
