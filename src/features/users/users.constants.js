export const INITIAL_USERS_STATE = {
  items: [],
  error: '',
};

export const USERS_NAMESPACE = 'users';
