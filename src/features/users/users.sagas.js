import { take, takeEvery, takeLatest, call, put, fork } from 'redux-saga/effects';
import { USERS_ACTIONS, getUsersSuccess, setUsersError } from './users.actions';
import { getUsers, createUser, deleteUser } from './users.api';

// Worker saga
// Reason: call API
// Implementation: try catch, promise call, actions put
// Result: calls API, returns data or error
function* getUsersSaga() {
  try {
    const result = yield call(getUsers);
    yield put(getUsersSuccess(result.data.data));
  } catch (e) {
    yield put(setUsersError('An error occurred when trying to get the users'));
  }
}

// Non-blocking watcher saga
// Reason: watch for specific action dispatch
// Implementation: takeEvery helper with watcher saga
// Result: calls a worker saga each time action dispatched
function* watchGetUsersRequest() {
  yield takeEvery(USERS_ACTIONS.GET_USERS_REQUEST, getUsersSaga);
}

function* deleteUserSaga(userId) {
  try {
    yield call(deleteUser, userId);
    yield call(getUsersSaga);
  } catch (e) {
    yield put(setUsersError('An error occurred when trying to delete the user'));
  }
}

// Blocking watcher saga
// Reason: avoid premature worker saga re-call (delete already deleted user)
// Implementation: while loop, take helper, watcher saga call
// Result: prevents worker saga running before previous call resolved
function* watchDeleteUserRequest() {
  while (true) {
    const { userId } = yield take(USERS_ACTIONS.DELETE_USER_REQUEST);
    yield call(deleteUserSaga, userId);
  }
}

function* createUserSaga({ user }) {
  try {
    yield call(createUser, user);
    yield call(getUsersSaga);
  } catch (e) {
    yield put(setUsersError('An error occurred when trying to create the user'));
  }
}

// Non-blocking watcher saga
// Reason: watch for last action dispatch
// Implementation: takeLatest helper with watcher saga
// Result: calls a worker saga on last action dispatched
function* watchCreateUserRequest() {
  yield takeLatest(USERS_ACTIONS.CREATE_USER_REQUEST, createUserSaga);
}

// Forked watcher sagas processes
// Reason: separate root saga children into independent processes
// Implementation: forked watcher sagas array
// Result: worker sagas forked from root saga & run in parallel
const usersSagas = [
  fork(watchGetUsersRequest),
  fork(watchDeleteUserRequest),
  fork(watchCreateUserRequest),
];

export default usersSagas;
