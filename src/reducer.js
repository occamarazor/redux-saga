import { combineReducers } from 'redux';
import { usersReducer } from './features/users';

export default combineReducers({
  users: usersReducer,
});
