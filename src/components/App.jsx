import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Alert } from 'reactstrap';
import {
  getUsersRequest,
  createUserRequest,
  deleteUserRequest,
  setUsersError,
  selectUsers,
} from '../features/users';
import { UserForm, UserList } from './users';

const App = () => {
  const dispatch = useDispatch();
  const { items, error } = useSelector(selectUsers);

  useEffect(() => {
    dispatch(getUsersRequest());
  }, [dispatch]);

  const handleUserSubmit = (user) => {
    dispatch(createUserRequest(user));
  };

  const handleDeleteUserClick = (userId) => {
    dispatch(deleteUserRequest(userId));
  };

  const handleCloseAlert = () => {
    dispatch(setUsersError(''));
  };

  return (
    <div style={{ margin: '0 auto', padding: '20px', maxWidth: '600px' }}>
      <h2>Users</h2>
      <Alert color='danger' isOpen={!!error} toggle={handleCloseAlert}>
        {error}
      </Alert>
      <UserForm onSubmit={handleUserSubmit} />
      {!!items && !!items.length && (
        <UserList onDeleteUserClick={handleDeleteUserClick} users={items} />
      )}
    </div>
  );
};

export default App;
