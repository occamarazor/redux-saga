import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import UserListItem from './UserListItem';

const UserList = ({ users, onDeleteUserClick }) => (
  <ListGroup>
    {users
      .sort((a, b) => {
        if (a.firstName > b.firstName) {
          return 1;
        }
        if (a.firstName < b.firstName) {
          return -1;
        }
        if (a.lastName > b.lastName) {
          return 1;
        }
        if (a.lastName < b.lastName) {
          return -1;
        }
        return 0;
      })
      .map((user) => (
        <ListGroupItem key={user.id}>
          <UserListItem onDeleteClick={onDeleteUserClick} user={user} />
        </ListGroupItem>
      ))}
  </ListGroup>
);

export default UserList;
